﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	
	public static GameManager Instance;

	public float LandBlocksSpeed = 10;

	private void Awake()
	{
		Instance = this;
	}

	private void Update()
	{
		if(Application.platform == RuntimePlatform.OSXEditor){
			if(Input.GetKeyDown(KeyCode.R)) {
				Reset();
			}
		} else {
			if(Input.GetKeyDown(KeyCode.Escape)) {
				Reset();
			}
		}
	}

	public void Reset()
	{
		SceneManager.LoadScene(0);
	}

}
