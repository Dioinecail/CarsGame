﻿using UnityEngine;

public class SpawnManager : MonoBehaviour
{

    public static SpawnManager Instance;
    public GameObject[] Path;
    public GameObject obstacle;

    void Awake()
    {
        Instance = this;
    }

    public void SpawnPath()
    {
        int rnd = Random.Range(0, Path.Length);

        Instantiate(Path[rnd], transform.position, Path[rnd].transform.rotation);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireCube(transform.position, Vector3.one * 5);
    }
}
