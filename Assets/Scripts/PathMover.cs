﻿using UnityEngine;

public class PathMover : MonoBehaviour {
	
    float Speed;

    private void Start()
    {
        Speed = GameManager.Instance.LandBlocksSpeed;
    }

    void Update ()
    {
        transform.position += (Vector3.down * Speed * Time.deltaTime);
        if(transform.position.y < -38)
        {
            SpawnManager.Instance.SpawnPath();
            Destroy(gameObject);
        }
	}    
}
