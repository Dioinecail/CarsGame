﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayTimeControl : MonoBehaviour {

	public float timeSpeed;

	private void Update()
	{
		transform.rotation = Quaternion.Euler(65.786f, -Time.time * timeSpeed, 0);
	}
}
