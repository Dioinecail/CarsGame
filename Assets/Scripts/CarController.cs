﻿using UnityEngine;

public class CarController : MonoBehaviour
{
    private Rigidbody rBody;
	public float speed;

	public float turnSpeed;
	public float returnSpeed;

	public string pointMaskName;
	LayerMask pointMask;

	Vector3 lastRaycastHitPosition;

	void Start ()
    {
        rBody = GetComponent<Rigidbody>();
		pointMask = LayerMask.GetMask(pointMaskName);
	}
	
	void Update ()
    {
        Move(GetMouseCoordinate());
	}

    private void Move(Vector3 target)
    {
		Vector3 moveDirection = target - transform.position;
		moveDirection.z = 0;

        Vector3 direction = Vector3.zero; 
		direction = Vector3.Normalize(moveDirection);

		Vector3 lookDirection = Vector3.up;

		if (direction != Vector3.zero && direction.y >= 0) {
			lookDirection = direction;
		}

		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookDirection, Vector3.back), (lookDirection == Vector3.up ? returnSpeed : turnSpeed) * Time.deltaTime);
		rBody.AddForce(direction * speed * Time.deltaTime * 100, ForceMode.Force);
    }

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(GetMouseCoordinate(), 1);
	}

	Vector3 GetMouseCoordinate() {
		Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit; 
		if (Physics.Raycast(mouseRay, out hit, 25, pointMask)) {
			lastRaycastHitPosition = hit.point;	
		}
		return lastRaycastHitPosition;
	}
}
