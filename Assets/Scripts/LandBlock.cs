﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LandBlock : MonoBehaviour
{
    public int sizeX, sizeY;
    public int obstacleZoneMin, obstacleZoneMax;
    public Vector3 offset;

    [Range(0, 100)]
    public int obstaclePercentage;

	// Use this for initialization
	void Start ()
    {
        offset = new Vector3(-sizeX / 2, -sizeY / 2, -0.5f);

        if(Application.isPlaying)
            GenerateObstacles();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(!Application.isPlaying)
            transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y), Mathf.RoundToInt(transform.position.z));
	}

    public void GenerateObstacles()
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                if((x <= obstacleZoneMin || x >= obstacleZoneMax) && Random.Range(0, 101) < obstaclePercentage)
                {
                    // generate obstacle there
                    GameObject obstacle = Instantiate(SpawnManager.Instance.obstacle, transform.position + offset + new Vector3(x, y), Quaternion.identity, transform);
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
		offset = new Vector3(-sizeX / 2, -sizeY / 2, -0.5f);
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                if (x <= obstacleZoneMin || x >= obstacleZoneMax)
                    Gizmos.color = Color.green;
                else
                    Gizmos.color = Color.yellow;

                Gizmos.DrawWireCube(transform.position + offset + new Vector3(x, y), Vector3.one);
            }
        }
    }
}
